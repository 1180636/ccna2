version 15.0
no service pad
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname MLS1-ZG
!
boot-start-marker
boot-end-marker
!
enable secret 5 $1$969L$.Vba7YZtdh8R7OcV97yfg/
!
username cisco secret 5 $1$ml.M$nkwQQMeQaU6jZqkazm/D.1
no aaa new-model
switch 1 provision ws-c3750e-24pd
system mtu routing 1500
ip routing
!
!
no ip domain-lookup
ip domain-name isepacademy.ccna.itn.com
!
!
crypto pki trustpoint TP-self-signed-1963290880
 enrollment selfsigned
 subject-name cn=IOS-Self-Signed-Certificate-1963290880
 revocation-check none
 rsakeypair TP-self-signed-1963290880
!
!
crypto pki certificate chain TP-self-signed-1963290880
 certificate self-signed 01
  3082022B 30820194 A0030201 02020101 300D0609 2A864886 F70D0101 05050030
  31312F30 2D060355 04031326 494F532D 53656C66 2D536967 6E65642D 43657274
  69666963 6174652D 31393633 32393038 3830301E 170D3036 30313032 30303031
  34315A17 0D323030 31303130 30303030 305A3031 312F302D 06035504 03132649
  4F532D53 656C662D 5369676E 65642D43 65727469 66696361 74652D31 39363332
  39303838 3030819F 300D0609 2A864886 F70D0101 01050003 818D0030 81890281
  8100F644 7347D134 033D8CFB DC9FB970 4C3C5E02 BFF406A2 EE01E1C0 BD934FB0
  53A8B5A5 F29522DB 31474B7B E09BE165 647911AC 8817BDBC 418D2AE1 4A470FAD
  EC9D0E7D 1068BA7F F36E4EB4 61C0B958 41F66A68 09A25416 2AFDD358 9207ABDE
  2CCD5085 9A0B4F99 03729DC1 E98BE85B 25632A15 0EA62405 268C1659 1EC78917
  A0BF0203 010001A3 53305130 0F060355 1D130101 FF040530 030101FF 301F0603
  551D2304 18301680 14BB3FEC 1F00C5C1 E45BD279 E120287A 01DE0250 63301D06
  03551D0E 04160414 BB3FEC1F 00C5C1E4 5BD279E1 20287A01 DE025063 300D0609
  2A864886 F70D0101 05050003 8181003B 6846CAC3 8BCAA996 E078B4BF DDAE0A41
  662D94D0 EA7F2333 B8A098FC C96C12DA C7EBDF04 EE17AFDD CB9864DA D422D794
  262B4A50 8C884723 B2079E4F 1A270AE1 DCFB6149 2EFB88C8 BA97A83D 6C9E1AAF
  65DE368B BE0604A2 73674EB7 6E1E638A 701A374C F3AA1762 F96EAA99 3B7A8E2F
  D4BE8E5A 931CDBFA 6984CCD4 BC3F30
        quit
!
spanning-tree mode rapid-pvst
spanning-tree extend system-id
spanning-tree vlan 5-6,10,20,30 priority 24576
spanning-tree vlan 40,50,60 priority 28672
!
!
!
!
!
!
!
!
!
vlan internal allocation policy ascending
!
!
!
!
!
!
!
!
!
!
!
interface Port-channel1
 switchport access vlan 99
 switchport trunk encapsulation dot1q
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5-7,10,20,30,40,50,60
 switchport mode trunk
!
interface FastEthernet0
 no ip address
 no ip route-cache
!
interface GigabitEthernet1/0/1
 description Connects Uplink to RT1
 no switchport
 ip address 172.20.7.98 255.255.255.252
!
interface GigabitEthernet1/0/2
 switchport trunk encapsulation dot1q
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5-7,10,20,30,40,50,60
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/3
 switchport trunk encapsulation dot1q
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5-7,10,20,30,40,50,60
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/4
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5-7,10,20,30,40,50,60
!
interface GigabitEthernet1/0/5
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5-7,10,20,30,40,50,60
!
interface GigabitEthernet1/0/6
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/7
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/8
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/9
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/10
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/11
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/12
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/13
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/14
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/15
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/16
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/17
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/18
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/19
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/20
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/21
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/22
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/23
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/24
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet1/0/25
!
interface GigabitEthernet1/0/26
!
interface GigabitEthernet1/0/27
!
interface GigabitEthernet1/0/28
!
interface TenGigabitEthernet1/0/1
!
interface TenGigabitEthernet1/0/2
!
interface Vlan1
 no ip address
!
interface Vlan5
 ip address 172.20.7.65 255.255.255.224
 standby 5 ip 172.20.7.94
 standby 5 priority 150
 standby 5 preempt
!
interface Vlan6
 ip address 172.20.7.1 255.255.255.224
 standby 6 ip 172.20.7.30
 standby 6 priority 150
 standby 6 preempt
!
interface Vlan10
 ip address 172.20.7.33 255.255.255.224
 standby 10 ip 172.20.7.62
 standby 10 priority 150
 standby 10 preempt
!
interface Vlan20
 ip address 172.20.4.1 255.255.255.0
 standby 20 ip 172.20.4.254
 standby 20 priority 150
 standby 20 preempt
!
interface Vlan30
 ip address 172.20.5.1 255.255.255.0
 standby 30 ip 172.20.5.254
 standby 30 priority 150
 standby 30 preempt
!
interface Vlan40
 ip address 172.20.0.1 255.255.254.0
 standby 40 ip 172.20.1.254
!
interface Vlan50
 ip address 172.20.2.1 255.255.254.0
 standby 50 ip 172.20.3.254
!
interface Vlan60
 ip address 172.20.6.1 255.255.255.0
 standby 60 ip 172.20.6.254
!
ip http server
ip http secure-server
!
!
!
!
banner motd ^C **************************************************************************************************************************

                     YOU SHOULDN'T PASS!!!!

=============================================================
                 WARNING TO UNAUTHORIZED USERS:

This system is for use by authorized users only. Any individual using this system, by such use, acknowledges and consents to the right of the company to monitor, access, use, and disclose any information generated, received, or stored on the systems.
************************************************************************************************************************** ^C
!
line con 0
 exec-timeout 60 0
 login local
line vty 0 4
 exec-timeout 60 0
 login local
 transport input ssh
line vty 5 15
 exec-timeout 60 0
 login local
 transport input ssh
!
end
