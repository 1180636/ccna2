version 15.4
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname RT1-ZG
!
boot-start-marker
boot-end-marker
!
!
security passwords min-length 10
enable secret 5 $1$HxJg$WFhsQF6ljqcQl74E/40s70
!
no aaa new-model
memory-size iomem 5
!
!
!
!
!
!
!
!
!
!
!
!
!
!
no ip domain lookup
ip domain name isepacademy.ccna.itn.com
ip cef
login quiet-mode access-class myacl
ipv6 unicast-routing
ipv6 cef
!
multilink bundle-name authenticated
!
!
!
!
!
!
cts logging verbose
!
!
voice-card 0
!
!
!
!
!
!
!
!
license udi pid CISCO2911/K9 sn FCZ160270GA
license accept end user agreement
license boot module c2900 technology-package securityk9
license boot module c2900 technology-package uck9
!
!
username cisco secret 5 $1$WAuS$3sy676br7sgSXPG058IVl1
!
redundancy
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
interface Embedded-Service-Engine0/0
 no ip address
 shutdown
!
interface GigabitEthernet0/0
 description Connects to MLS1
 ip address 172.20.7.97 255.255.255.252
 duplex auto
 speed auto
!
interface GigabitEthernet0/1
 description Connects to MLS2
 ip address 172.20.7.101 255.255.255.252
 duplex auto
 speed auto
!
interface GigabitEthernet0/2
 description Connects to SW0
 no ip address
 duplex auto
 speed auto
!
interface GigabitEthernet0/2.102
 description Connection to RT1-PL
 encapsulation dot1Q 102
 ip address 10.0.0.1 255.255.255.252
!
interface GigabitEthernet0/2.103
 description Connection to RT1-ST
 encapsulation dot1Q 103
 ip address 10.0.0.5 255.255.255.252
!
interface Serial0/0/0
 no ip address
 shutdown
 clock rate 2000000
!
interface Serial0/0/1
 no ip address
 shutdown
 clock rate 2000000
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
ip route 172.20.8.0 255.255.252.0 GigabitEthernet0/2.102 10.0.0.2
ip route 172.20.8.0 255.255.252.0 GigabitEthernet0/2.103 10.0.0.6 5
ip route 172.20.12.0 255.255.254.0 GigabitEthernet0/2.103 10.0.0.6
ip route 172.20.12.0 255.255.254.0 GigabitEthernet0/2.102 10.0.0.2 5
!
!
!
!
control-plane
!
 !
 !
 !
 !
!
mgcp behavior rsip-range tgcp-only
mgcp behavior comedia-role none
mgcp behavior comedia-check-media-src disable
mgcp behavior comedia-sdp-force disable
!
mgcp profile default
!
!
!
!
!
!
!
gatekeeper
 shutdown
!
!
banner motd ^C **************************************************************************************************************************

                     YOU SHOULDN'T PASS!!!!

=============================================================
                 WARNING TO UNAUTHORIZED USERS:

This system is for use by authorized users only. Any individual using this system, by such use, acknowledges and consents to the right of the company to monitor, access, use, and disclose any information generated, received, or stored on the systems.
************************************************************************************************************************** ^C
!
line con 0
 exec-timeout 60 0
 login local
line aux 0
line 2
 no activation-character
 no exec
 transport preferred none
 transport output pad telnet rlogin lapb-ta mop udptn v120 ssh
 stopbits 1
line vty 0 4
 exec-timeout 60 0
 login local
 transport input ssh
line vty 5 15
 exec-timeout 60 0
 login local
 transport input ssh
!
scheduler allocate 20000 1000
!
end
