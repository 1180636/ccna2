version 15.0
no service pad
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname SW2-ZG
!
boot-start-marker
boot-end-marker
!
enable secret 5 $1$N0dX$ZJP87hHYBkfdAOec7kpy51
!
username cisco secret 5 $1$ixcF$ksiBcX15vASCuPXQay0Gw/
no aaa new-model
system mtu routing 1500
!
!
no ip domain-lookup
ip domain-name isepacademy.ccna.itn.com
!
!
!
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
vlan internal allocation policy ascending
!
!
!
!
!
!
interface FastEthernet0/1
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/2
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/3
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/4
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/5
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/6
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/7
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/8
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/9
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/10
 switchport access vlan 20
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/11
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/12
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/13
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/14
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/15
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/16
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/17
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/18
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/19
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/20
 switchport access vlan 30
 switchport mode access
 switchport voice vlan 40
 mls qos trust cos
 spanning-tree portfast
!
interface FastEthernet0/21
 switchport access vlan 5
 switchport mode access
!
interface FastEthernet0/22
 switchport access vlan 5
 switchport mode access
!
interface FastEthernet0/23
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5,6,50,60
 switchport mode trunk
!
interface FastEthernet0/24
 switchport access vlan 99
 switchport mode access
 shutdown
!
interface GigabitEthernet0/1
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5-7,10,20,30,40,50,60
 switchport mode trunk
!
interface GigabitEthernet0/2
 switchport trunk native vlan 7
 switchport trunk allowed vlan 5-7,10,20,30,40,50,60
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan6
 ip address 172.20.7.5 255.255.255.224
!
ip default-gateway 172.20.7.30
ip http server
ip http secure-server
!
vstack
banner motd ^C **************************************************************************************************************************

                     YOU SHOULDN'T PASS!!!!

=============================================================
                 WARNING TO UNAUTHORIZED USERS:

This system is for use by authorized users only. Any individual using this system, by such use, acknowledges and consents to the right of the company to monitor, access, use, and disclose any information generated, received, or stored on the systems.
************************************************************************************************************************** ^C
!
line con 0
 exec-timeout 60 0
 login local
line vty 0 4
 exec-timeout 0 0
 login local
 transport input ssh
line vty 5 15
 exec-timeout 0 0
 login local
 transport input ssh
!
end
