hostname MLS1-ZG
no ip domain-lookup
no logging console
ip domain-name isepacademy.ccna.itn.com
enable secret classclass
username cisco secret classclass
service password-encryption
crypto key generate rsa
1024
ip routing
line con 0
exec-timeout 60
login local
line vty 0 15
transport input ssh
login local
exec-timeout 60
vlan 5
name APs
Vlan 6
name MANAGEMENT
vlan 7
name NATIVE
vlan 10
name SERVERS
vlan 20
name ADMINISTRATION
vlan 30
name STAFF
vlan 40
name VOIP
vlan 50
name WIRELESS
vlan 60
name GUEST
vlan 99
name BLACKHOLE
exit

spanning-tree mode rapid-pvst
spanning-tree vlan 5,6,10,20,30 root primary
spanning-tree vlan 40,50,60 root secondary

banner motd # **************************************************************************************************************************

                     YOU SHOULDN'T PASS!!!!

=============================================================
                 WARNING TO UNAUTHORIZED USERS:

This system is for use by authorized users only. Any individual using this system, by such use, acknowledges and consents to the right of the company to monitor, access, use, and disclose any information generated, received, or stored on the systems.
************************************************************************************************************************** #

interface range g1/0/2 - 3
channel-group 1 mode on
exi
int port-channel 1
Switchport trunk encapsulation dot1q
switchport mode trunk
Switchport trunk native vlan 7
switchport trunk allowed vlan 5,6,7,10,20,30,40,50,60
No shut
Exit

int r g1/0/4 - 5
Switchport mode trunk
Switchport trunk native vlan 7
Switchport trunk allowed vlan 5,6,7,10,20,30,40,50,60
No shut
Exit

Int r g1/0/6-24
Switchport mode access
Switchport access vlan 99
Shut
Exit

interface GigabitEthernet1/0/1
 description Connects Uplink to RT1
 no shut
 no switchport
 ip address 172.20.7.98 255.255.255.252
 duplex auto
 speed auto

interface vlan5
 ip address 172.20.7.65 255.255.255.224
 standby 5 ip 172.20.7.94
 standby 5 priority 150
 standby 5 preempt
 
interface vlan6
 ip address 172.20.7.1 255.255.255.224
 standby 6 ip 172.20.7.30
 standby 6 priority 150
 standby 6 preempt
 
interface vlan10
 ip address 172.20.7.33 255.255.255.224
 standby 10 ip 172.20.7.62
 standby 10 priority 150
 standby 10 preempt

interface vlan20
 ip address 172.20.4.1 255.255.255.0
 standby 20 ip 172.20.4.254
 standby 20 priority 150
 standby 20 preempt

interface vlan30
 ip address 172.20.5.1 255.255.255.0
 standby 30 ip 172.20.5.254
 standby 30 priority 150
 standby 30 preempt
 
interface vlan40
 ip address 172.20.0.1 255.255.254.0
 standby 40 ip 172.20.1.254
 
interface vlan50
 ip address 172.20.2.1 255.255.254.0
 standby 50 ip 172.20.3.254
 
interface vlan60
 ip address 172.20.6.1 255.255.255.0
 standby 60 ip 172.20.6.254
 
ip route 0.0.0.0 0.0.0.0 172.20.7.97